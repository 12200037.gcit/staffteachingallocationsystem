<?php

require 'includes/connect.php';
require 'includes/user.php';

// $con = getDB();

if (isset($_GET['userId'])) {
    $user = getUser($con, $_GET['userId']);

    if ($user) {
        $email = $user['email'];
        $employee_id = $user['employee_id'];
        $position = $user['position'];
        $password = $user['password'];
    } else {
        die("article not found");
    }
} else {
    die("id not supplied, user not found");
}
?>
<?php require 'includes/sidebar.php' ?>
<?php require 'includes/addUser.php' ?>
