<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require 'includes/connect.php';

    // Check if the user_id is set and not empty
    if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
        $user_id = $_POST['user_id'];

        $sql = "DELETE FROM users WHERE userId = ?";
        if ($stmt = $con->prepare($sql)) {
            $stmt->bind_param("i", $user_id);
            if ($stmt->execute()) {
                // User deleted successfully
                echo "User deleted successfully";
            } else {
                echo "Error: " . $stmt->error;
            }
            $stmt->close();
        } else {
            echo "Error: " . $con->error;
        }
    } else {
        echo "User ID not provided or invalid.";
    }

    $con->close();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <!-- font awesome cdn link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="./css/style.css">
    <title>Users</title>
</head>

<body>
    <?php require "includes/addUser.php" ?>
    <?php
    require "includes/sidebar.php";
    ?>
    <div class="container outer">

        <div class="header-user mt-3">
            <h4 class="text-dark">Users</h4>
        </div>
        <div class="add-container mt-3 row">
            <div class="col">
                <h6 class="user-list">Users List</h6>
            </div>
            <div class="col text-end">
                <button class="btn add-user" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Add</button>
            </div>
        </div>
        <div class="table-responsive mt-3">
            <table class="table table-responsive table-borderless userTable">

                <thead>
                    <tr class="bg-light">
                        <th scope="col" width="5%"><input class="form-check-input" type="checkbox"></th>
                        <th scope="col" width="5%">#</th>
                        <th scope="col" width="20%">Email ID</th>
                        <th scope="col" width="20%">Employee ID</th>
                        <th scope="col" width="20%">Postion</th>
                        <th scope="col" width="20%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    require 'includes/connect.php';

                    $sql = "SELECT * FROM users";
                    $result = $con->query($sql);

                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            echo '<tr>';
                            echo '<th scope="row"><input class="form-check-input" type="checkbox"></th>';
                            echo '<td>' . $row['userId'] . '</td>';
                            echo '<td>' . $row['email'] . '</td>';
                            echo '<td>' . $row['employee_id'] . '</td>';
                            echo '<td>' . $row['position'] . '</td>';
                            echo '<td>';
                            echo '<span><button class="btn text-success">Edit</button></span>';
                            echo '<button class="btn text-danger delete-user" data-userid="' . $row['userId'] . '">Delete</button>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    } else {
                        echo '<tr><td colspan="6">No users found</td></tr>';
                    }

                    $con->close();
                    ?>
                </tbody>
            </table>
        </div>
    </div>


</body>

</html>