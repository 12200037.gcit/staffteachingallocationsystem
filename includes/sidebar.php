<?php
session_start();
if (!isset($_SESSION['email'])) {
    header('location:Signin.php');
}
?>
</div>
<div class="sidebar">
    <div class="logo">
        <h4>GCIT</h4>
        <p>Staff Teaching Allocation</p>
    </div>
    <ul class="menu">
        <li>
            <a href="dashboard.php">
                <i class="bx bxs-dashboard" id="dashboard-icon"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="Users.php">
                <i class="bx bx-user" id="user-icon"></i>
                <span>Users</span>
            </a>
        </li>
        <li>
            <a href="modules.php">
                <i class="bx bxs-message-rounded-error" id="issue-icon"></i>
                <span>Modules</span>
            </a>
        </li>
        <li>
            <a href="specialization.php">
                <i class="bx bxs-message-alt-detail" id="feedback-icon"></i>
                <span>Specialization</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="bx bxs-message-alt-detail" id="feedback-icon"></i>
                <span>Registry</span>
            </a>
        </li>
        <li>
            <a href="profile.php">
                <i class="bx bxs-message-alt-detail" id="feedback-icon"></i>
                <span>Profile</span>
            </a>
        </li>
        <li class="logout">
            <a href="Signout.php">
                <i class="bx bxs-log-out"></i>
                <span>Sign Out</span>
            </a>
        </li>
    </ul>
</div>
<div class="user-email"> <?php echo $_SESSION['email']; ?>