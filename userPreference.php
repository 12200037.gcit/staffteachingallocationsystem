<?php
session_start();
if (!isset($_SESSION['email'])) {
    header('location:Signin.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Users</title>
</head>

<body>
    <div class="sidebar">
        <div class="logo">
            <h4>GCIT</h4>
            <p>Staff Teaching Allocation</p>
        </div>
        <ul class="menu">
            <li>
                <a href="#">
                    <i class="bx bxs-dashboard" id="dashboard-icon"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="active">
                <a href="Users.php">
                    <i class="bx bx-user" id="user-icon"></i>
                    <span>Users</span>
                </a>
            </li>
            <li>
                <a href="modules.php">
                    <i class="bx bxs-message-rounded-error" id="issue-icon"></i>
                    <span>Modules</span>
                </a>
            </li>
            <li>
                <a href="specialization.php">
                    <i class="bx bxs-message-alt-detail" id="feedback-icon"></i>
                    <span>Specialization</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="bx bxs-message-alt-detail" id="feedback-icon"></i>
                    <span>Registry</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="bx bxs-message-alt-detail" id="feedback-icon"></i>
                    <span>Profile</span>
                </a>
            </li>
            <li class="logout">
                <a href="Signout.php">
                    <i class="bx bxs-log-out"></i>
                    <span>Sign Out</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="user-email"> <?php echo $_SESSION['email']; ?>
    </div>
    <div class="container outer">
        <div class="header-user mt-3">
            <h4>Users</h4>
        </div>
        <div class="add-container mt-3 row">
            <div class="col">
                <a href="modules.php" class='link'>Modules</a>
                <a href="userPreference.php" class="link">User Preference</a>
            </div>
            <div class="col text-end">
                <button class="btn add-user">Add</button>
            </div>
        </div>
        <div class="table-responsive mt-3">
            <table class="table table-responsive table-borderless">

                <thead>
                    <tr class="bg-light">
                        <th scope="col" width="5%"><input class="form-check-input" type="checkbox"></th>
                        <th scope="col" width="5%">#</th>
                        <th scope="col" width="20%">Title</th>
                        <th scope="col" width="15%%">Code</th>
                        <th scope="col" width="15%%">Year</th>
                        <th scope="col" width="20%">Specialization</th>
                        <th scope="col" width="30%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row"><input class="form-check-input" type="checkbox"></th>
                        <td>1</td>
                        <td>karma.gcit@rub.edu.bt</td>
                        <td>12200052</td>
                        <td>Admin</td>
                        <td>AI</td>
                        <td><span><button class="btn text-success">Edit</button></span><span><button class="btn text-danger">Delete</button></span></td>
                    </tr>
                    <tr>
                        <th scope="row"><input class="form-check-input" type="checkbox"></th>
                        <td>2</td>
                        <td>karma.gcit@rub.edu.bt</td>
                        <td>12200052</td>
                        <td>Admin</td>
                        <td>AI</td>
                        <td><span><button class="btn text-success">Edit</button></span><span><button class="btn text-danger">Delete</button></span></td>
                    </tr>
                    <tr>
                        <th scope="row"><input class="form-check-input" type="checkbox"></th>
                        <td>3</td>
                        <td>karma.gcit@rub.edu.bt</td>
                        <td>12200052</td>
                        <td>Admin</td>
                        <td>AI</td>
                        <td><span><button class="btn text-success">Edit</button></span><span><button class="btn text-danger">Delete</button></span></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</body>

</html>