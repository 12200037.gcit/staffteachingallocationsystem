<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Users</title>
</head>

<body>
    <?php require "includes/sidebar.php"; ?>
    <div class="container outer">
        <div class="header-user mt-3">
            <h4 class="text-dark">Specialization</h4>
        </div>
        <div class="add-container mt-3 row">
            <div class="col">
                <h6 class="user-list">Specialization</h6>
            </div>
            <div class="col text-end">
                <button class="btn add-user">Add</button>
            </div>
        </div>
        <div class="table-responsive mt-3">
            <table class="table table-responsive table-borderless">

                <thead>
                    <tr class="bg-light">
                        <th scope="col" width="5%"><input class="form-check-input" type="checkbox"></th>
                        <th scope="col" width="5%">#</th>
                        <th scope="col" width="20%">Title</th>
                        <th scope="col" width="20%">School</th>
                        <th scope="col" width="20%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row"><input class="form-check-input" type="checkbox"></th>
                        <td>1</td>
                        <td>karma.gcit@rub.edu.bt</td>
                        <td>12200052</td>
                        <td><span><button class="btn text-success">Edit</button></span><span><button class="btn text-danger">Delete</button></span></td>
                    </tr>
                    <tr>
                        <th scope="row"><input class="form-check-input" type="checkbox"></th>
                        <td>2</td>
                        <td>karma.gcit@rub.edu.bt</td>
                        <td>12200052</td>
                        <td><span><button class="btn text-success">Edit</button></span><span><button class="btn text-danger">Delete</button></span></td>
                    </tr>
                    <tr>
                        <th scope="row"><input class="form-check-input" type="checkbox"></th>
                        <td>3</td>
                        <td>karma.gcit@rub.edu.bt</td>
                        <td>12200052</td>
                        <td><span><button class="btn text-success">Edit</button></span><span><button class="btn text-danger">Delete</button></span></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</body>

</html>