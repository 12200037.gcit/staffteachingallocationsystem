<?php
require 'includes/connect.php';
require 'includes/session.php';


$sql = "SELECT * FROM users";

$results = mysqli_query($con, $sql);

if ($results === false) {
    echo mysqli_error($con);
} else {
    $users = mysqli_fetch_all($results, MYSQLI_ASSOC);
}
?>

<?php require "includes/header.php"; ?>
<?php require "addUser.php" ?>
<?php require "includes/sidebar.php"; ?>

<div class="container outer">
    <div class="header-user mt-3">
        <h4 class="text-dark">Users</h4>
    </div>
    <div class="add-container mt-3 row">
        <div class="col">
            <h6 class="user-list">Users List</h6>
        </div>
        <div class="col text-end">
            <button class="btn add-user" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Add</button>
        </div>
    </div>
    <div class="table-responsive mt-3">
        <table class="table table-responsive table-borderless userTable">

            <thead>
                <tr class="bg-light">
                    <th scope="col" width="5%"><input class="form-check-input" type="checkbox"></th>
                    <th scope="col" width="5%">#</th>
                    <th scope="col" width="20%">Email ID</th>
                    <th scope="col" width="20%">Employee ID</th>
                    <th scope="col" width="20%">Postion</th>
                    <th scope="col" width="20%">Action</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($users as $user) : ?>
                    <tr>
                        <!-- <user> -->
                        <td scope="col" width="5%"><input class="form-check-input" type="checkbox"></td>
                        <td><?= $user['userId']; ?></td>
                        <td><?= $user['email']; ?></td>
                        <td><?= $user['employee_id']; ?></td>
                        <td><?= $user['position']; ?></td>
                        <td>
                            <button class="btn"><a class="text-success" href="editUser.php?userId=<?= $user['userId']; ?>">Edit</a></button>
                            <button class="btn"><a class="text-danger" href="deleteUser.php?userId=<?= $user['userId']; ?>">Delete</a></button>
                        </td>
                        <!-- </user> -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php require "includes/footer.php"; ?>