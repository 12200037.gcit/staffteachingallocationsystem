<?php
require 'includes/connect.php';
require 'includes/session.php';


$sql = "SELECT * FROM modules";

$results = mysqli_query($con, $sql);

if ($results === false) {
    echo mysqli_error($con);
} else {
    $modules = mysqli_fetch_all($results, MYSQLI_ASSOC);
}
?>

<?php require "includes/header.php"; ?>
<?php require "addModule.php" ?>
<?php require "includes/sidebar.php"; ?>

<div class="container outer">
    <div class="header-user mt-3">
        <h4 class="text-dark">Modules</h4>
    </div>
    <div class="add-container mt-3 row g-0">
        <div class="col">
            <!-- <h6 class="user-list"> Modules</h6> -->
            <a href="Modules.php" class="user-list">Modules</a>
        </div>
        <div class="col">
            <!-- <h6 class="user-list">User Preferences</h6> -->
            <a href="Modules.php" class="user-list">User Preferences</a>

        </div>
        <div class="col text-end">
            <button class="btn mail" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Add</button>
        </div>
        <div class="col text-end">
            <button class="btn add-user" type="button">Email</button>
        </div>
    </div>
    <div class="table-responsive mt-3">
        <table class="table table-responsive table-borderless userTable">

            <thead>
                <tr class="bg-light">
                    <th scope="col" width="5%"><input class="form-check-input" type="checkbox"></th>
                    <th scope="col" width="5%">#</th>
                    <th scope="col" width="20%">Module Title</th>
                    <th scope="col" width="20%">Module Code</th>
                    <th scope="col" width="10%%">Year</th>
                    <th scope="col" width="20%">Specailizaion</th>
                    <th scope="col" width="30%%">Actioss</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($modules as $module) : ?>
                    <tr>
                        <!-- <user> -->
                        <td scope="col" width="5%"><input class="form-check-input" type="checkbox"></td>
                        <td><?= $module['moduleId']; ?></td>
                        <td><?= $module['title']; ?></td>
                        <td><?= $module['code']; ?></td>
                        <td><?= $module['year']; ?></td>
                        <td><?= $module['specialization']; ?></td>
                        <td>
                            <button class="btn"><a class="text-success" href="editUser.php?moduleId=<?= $module['moduleId']; ?>">Edit</a></button>
                            <button class="btn"><a class="text-danger" href="deleteModule.php?moduleId=<?= $module['moduleId']; ?>">Delete</a></button>
                        </td>
                        <!-- </user> -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php require "includes/footer.php"; ?>