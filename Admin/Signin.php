<?php
$login = 0;
$invalid = 0;

require 'includes/connect.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    $email = $_POST['email'];
    $password = $_POST['password'];

    // Retrieve the hashed password from the database based on the email
    $sql = "SELECT password FROM users WHERE email='$email'";
    $result = mysqli_query($con, $sql);

    if ($result) {
        $row = mysqli_fetch_assoc($result);
        $storedHashedPassword = $row['password'];

        if (password_verify($password, $storedHashedPassword)) {
            // Passwords match, login successful
            $login = 1;
            session_start();
            $_SESSION['email'] = $email;
            header('location: Users.php');
        } else {
            // Passwords do not match, invalid login
            $invalid = 1;
        }
    }
}
?>
<?php require "includes/header.php"; ?>
<?php
if ($login) {
    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
   Login Successful
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>';
}
?>
<?php
if ($invalid) {
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
   invalid email or password!
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>';
}
?>
<div class="container-fluid p=0 vh-100 large-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 vh-50 mt-5 d-flex flex-column align-items-center left-container">
                <h5 class="text-light mt-5">GCIT</h5>
                <div style="width:70%">
                    <p class="text-light mt-5">Streamlining
                        Academic Progress: Your Personalized
                        Module Allocation System</p>
                </div>
            </div>
            <div class="col-lg-7 vh-50 d-flex flex-column align-items-center mt-5 right-container">
                <h5 class="mt-5">Sign In</h5>
                <form action="Signin.php" method="post" class="mt-5">
                    <div class="mb-3 col-xs-3">
                        <input type="text" class="form-control" name="email" placeholder="Email" style="width: 300px;">
                    </div>
                    <div class="mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Password" style="width: 300px;">
                    </div>
                    <button class="btn mb-5" style="width: 300px;">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<?php require "includes/footer.php"; ?>