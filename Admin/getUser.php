<?php
require 'includes/connect.php';
require 'includes/user.php';

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (isset($_POST["userId"])) {
        $userId = $_POST["userId"];
        $user = getUser($con, $userId);
        echo json_encode($user);
    } else {
        echo json_encode(["error" => "User ID not provided"]);
    }
}
