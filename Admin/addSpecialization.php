<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require 'includes/connect.php';
    require "includes/alert.php";

    $title = $_POST['title'];
    $school = $_POST['school'];


    $sql = "INSERT INTO specialization (title, school) VALUES (?, ?)";

    if ($stmt = $con->prepare($sql)) {
        $stmt->bind_param("ss", $title, $school);

        if ($stmt->execute()) {
            echo '<script>showAlert("Specailizatiom added successfully");</script>';
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "Error: " . $con->error;
    }
}

?>

<div class="modal fade" id="exampleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add users</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="addform" method="post">
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Specialization Title" autocomplete="off" required>
                    </div>
                    <div class="form-group mb-3">
                        <select name="school" id="school" type="text" class="form-select" placeholder="Position" required>
                            <option value="Computer Science">Computer Science</option>
                            <option value="Interactive Design and Development">Interactive Design and Development</option>
                            <option value="General">General</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary add-user">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>