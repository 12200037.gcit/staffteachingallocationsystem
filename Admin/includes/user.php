<?php

function getUser($con, $userId)
{
    $sql = "SELECT * FROM users WHERE userId = ?";

    $stmt = mysqli_prepare($con, $sql);

    if ($stmt === false) {
        echo mysqli_error($con);
    } else {
        mysqli_stmt_bind_param($stmt, "i", $userId);

        if (mysqli_stmt_execute($stmt)) {
            $result = mysqli_stmt_get_result($stmt);

            return mysqli_fetch_array($result, MYSQLI_ASSOC);
        }
    }
}
