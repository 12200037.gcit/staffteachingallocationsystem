<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
    <div class="alert-box" id="alert-box">
        <span class="close-button" id="close-button" onclick="closeAlert()">&times;</span>
        <p id="alert-message"></p>
    </div>
</body>
<script>
    function showAlert(message) {
        const alertBox = document.getElementById('alert-box');
        const alertMessage = document.getElementById('alert-message');

        alertMessage.innerHTML = message;
        alertBox.style.display = 'block';
    }

    function closeAlert() {
        const alertBox = document.getElementById('alert-box');
        alertBox.style.display = 'none';
    }
</script>

</html>

<style>
    .alert-box {
        display: none;
        position: fixed;
        top: 45%;
        left: 42.5%;
        width: 15%;
        height: 10%;
        padding: 10px;
        background: white;
        border: 1px solid #ccc;
        box-shadow: 0px 0px 10px #888;
        z-index: 9999;
    }

    .close-button {
        position: absolute;
        top: 5px;
        right: 10px;
        cursor: pointer;
    }

    .close-button:hover {
        color: red;
    }

    #alert-message {
        text-align: center;
    }
</style>