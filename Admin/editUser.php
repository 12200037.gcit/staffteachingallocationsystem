<?php

require 'includes/connect.php';
require "includes/alert.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $userId = $_GET['userId'];

    if (isset($_POST['submit'])) {
        $email = $_POST['email'];
        $employee_id = $_POST['employee_id'];
        $position = $_POST['position'];
        $password = $_POST['password'];

        $hashedPassword = password_hash($password, PASSWORD_BCRYPT);

        $sql = "UPDATE users SET userId=$userId, email = '$email', employee_id = '$employee_id', position = '$position', password = '$password' WHERE userId = $userId";
        $result = mysqli_query($con, $sql);
        if ($result) {
            // header('location:Users.php');

            echo "<script> alert('User successfully updated!'); </script>";
        } else {
            die(mysqli_error($con));
        }
    }
}

?>
<?php require "includes/header.php" ?>
<?php require "includes/sidebar.php" ?>
<div class="container outer">
    <div class=" userForm">
        <form id="editForm" method="post">
            <div class="modal-body">
                <div class="form-group mb-3">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" required>
                </div>
                <div class="form-group mb-3">
                    <input type="text" class="form-control" name="employee_id" id="employee_id" placeholder="Employee ID" autocomplete="off" required>
                </div>
                <div class="form-group mb-3">
                    <select name="position" id="position" type="text" class="form-select" placeholder="Position" required>
                        <option value=" admin">Admin</option>
                        <option value="team lead">Team Lead</option>
                        <option value="user">User</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <input type="password" class="form-control" name="password" id="password" placeholder=" Password" required minlength="8">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary add-user">Update</button>
            </div>
        </form>
    </div>
</div>
<?php require "includes/footer.php" ?>