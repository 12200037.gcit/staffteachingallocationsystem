<?php
$success = 0;
$user = 0;
$invalid = 0;

require './includes/connect.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    $email = $_POST['email'];
    $employee_id = $_POST['employee_id'];
    $position = $_POST['position'];
    $password = $_POST['password'];
    $cpassword = $_POST['cpassword'];

    $sql = "SELECT * FROM users WHERE email = ?";

    if ($stmt = $con->prepare($sql)) {
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            $user = 1;
        } else {
            if ($password === $cpassword) {
                $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
                $stmt->close();

                $sql = "INSERT INTO users (email, employee_id, position, password) VALUES (?, ?, ?, ?)";

                if ($stmt = $con->prepare($sql)) {
                    $stmt->bind_param("ssss", $email, $employee_id, $position, $hashedPassword);
                    if ($stmt->execute()) {
                        $success = 1;
                        header('location: Signin.php');
                    } else {
                        $invalid = 1;
                    }
                }
            }
        }
        $stmt->close();
    }
}
?>
<?php require "includes/header.php"; ?>
<?php
if ($user) {
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
   User already exists
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>';
}
?>
<?php
if ($success) {
    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
   Successfully registered! Please login to continue.
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>';
}
?>
<?php
if ($invalid) {
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
   Invalid Credentials
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>';
}
?>
<div class="container-fluid p=0 vh-100 large-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 vh-50 mt-5 d-flex flex-column align-items-center left-container">
                <h5 class="text-light mt-5">GCIT</h5>
                <div style="width:70%">
                    <p class="text-light mt-5">Streamlining
                        Academic Progress: Your Personalized
                        Module Allocation System</p>
                </div>
            </div>
            <div class="col-lg-7 vh-50 d-flex flex-column align-items-center mt-5 right-container">
                <h5 class="mt-5">Sign Up</h5>
                <form action="Signup.php" method="post" class="mt-5">
                    <div class="mb-3 col-xs-3">
                        <input type="text" class="form-control" name="email" placeholder="Email" style="width: 300px;" required>
                    </div>
                    <div class="mb-3 ">
                        <input type="text" class="form-control" name="employee_id" placeholder="Employee ID" style="width: 300px;" required>
                    </div>
                    <div class="mb-3">
                        <select name="position" type="text" class="form-select" placeholder="Position" style="width: 300px;" required>
                            <option selected>Position</option>
                            <option value="admin">Admin</option>
                            <option value="team lead">Team Lead</option>
                            <option value="user">User</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Password" style="width: 300px;" required minlength="8">
                    </div>
                    <div class="mb-3">
                        <input type="password" class="form-control" name="cpassword" placeholder="Confirm Password" style="width: 300px;" required>
                    </div>
                    <button class="btn mb-5" style="width: 300px;">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<?php require "includes/footer.php"; ?>