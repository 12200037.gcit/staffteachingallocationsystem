<?php
require 'includes/connect.php';
require 'includes/session.php';


$sql = "SELECT * FROM specialization";

$results = mysqli_query($con, $sql);

if ($results === false) {
    echo mysqli_error($con);
} else {
    $specializations = mysqli_fetch_all($results, MYSQLI_ASSOC);
}
?>

<?php require "includes/header.php"; ?>
<?php require "addSpecialization.php" ?>
<?php require "includes/sidebar.php"; ?>

<div class="container outer">
    <div class="header-user mt-3">
        <h4 class="text-dark">Specialization</h4>
    </div>
    <div class="add-container mt-3 row">
        <div class="col">
            <a href="Specialization.php" class="user-list">Specialization</a>
        </div>
        <div class="col text-end">
            <button class="btn add-user" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Add</button>
        </div>
    </div>
    <div class="table-responsive mt-3">
        <table class="table table-responsive table-borderless userTable">

            <thead>
                <tr class="bg-light">
                    <th scope="col" width="5%"><input class="form-check-input" type="checkbox"></th>
                    <th scope="col" width="5%">#</th>
                    <th scope="col" width="20%">Title</th>
                    <th scope="col" width="20%">School</th>
                    <th scope="col" width="20%">Action</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($specializations as $specialization) : ?>
                    <tr>
                        <!-- <user> -->
                        <td scope="col" width="5%"><input class="form-check-input" type="checkbox"></td>
                        <td><?= $specialization['specializationId']; ?></td>
                        <td><?= $specialization['title']; ?></td>
                        <td><?= $specialization['school']; ?></td>
                        <td>
                            <button class="btn"><a class="text-success" href="editUser.php?specializationId=<?= $specialization['specializationId']; ?>">Edit</a></button>
                            <button class="btn"><a class="text-danger" href="deleteSpecialization.php?specializationId=<?= $specialization['specializationId']; ?>">Delete</a></button>
                        </td>
                        <!-- </user> -->
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php require "includes/footer.php"; ?>