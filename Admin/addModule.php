<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require 'includes/connect.php';
    require "includes/alert.php";

    $title = $_POST['title'];
    $code = $_POST['code'];
    $year = $_POST['year'];
    $specialization = $_POST['specialization'];


    $sql = "INSERT INTO modules (title, code, year, specialization) VALUES (?, ?, ?, ?)";

    if ($stmt = $con->prepare($sql)) {
        $stmt->bind_param("ssis", $title, $code, $year, $specialization);

        if ($stmt->execute()) {
            echo '<script>showAlert("Module added successfully");</script>';
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "Error: " . $con->error;
    }
}

?>
<?php
$sql = "SELECT * FROM specialization";

$results = mysqli_query($con, $sql);

if ($results === false) {
    echo mysqli_error($con);
} else {
    $specializations = mysqli_fetch_all($results, MYSQLI_ASSOC);
}
?>
<div class="modal fade" id="exampleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add users</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="addform" method="post">
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Module Title" autocomplete="off" required>
                    </div>
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" name="code" id="code" placeholder="Module Code" autocomplete="off" required>
                    </div>
                    <div class="form-group mb-3">
                        <input type="year" class="form-control" name="year" id="year" placeholder="Year" autocomplete="off" required>

                    </div>
                    <div class="form-group mb-3">
                        <select name="specialization" id="specialization" class="form-select" placeholder="specialization" required>
                            <?php foreach ($specializations as $spec) : ?>
                                <option value="<?= $spec['specializationId']; ?>"><?= $spec['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary add-user">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>