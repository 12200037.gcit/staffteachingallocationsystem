<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require 'includes/connect.php';
    require "includes/alert.php";

    $email = $_POST['email'];
    $employee_id = $_POST['employee_id'];
    $position = $_POST['position'];
    $password = $_POST['password'];

    // Hash the password using password_hash
    $hashedPassword = password_hash($password, PASSWORD_BCRYPT);

    $sql = "INSERT INTO users (email, employee_id, position, password) VALUES (?, ?, ?, ?)";

    if ($stmt = $con->prepare($sql)) {
        $stmt->bind_param("ssss", $email, $employee_id, $position, $hashedPassword);

        if ($stmt->execute()) {
            echo '<script>showAlert("User created successfully");</script>';
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "Error: " . $con->error;
    }
}

?>

<div class="modal fade" id="exampleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add users</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="addform" method="post">
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" required>
                    </div>
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" name="employee_id" id="emploayee_id" placeholder="Employee ID" autocomplete="off" required>
                    </div>
                    <div class="form-group mb-3">
                        <select name="position" id="position" type="text" class="form-select" placeholder="Position" required>
                            <option selected>Position</option>
                            <option value=" admin">Admin</option>
                            <option value="team lead">Team Lead</option>
                            <option value="user">User</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required minlength="8">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary add-user">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>